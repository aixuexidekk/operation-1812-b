#!/usr/bin/env node
const commander = require('commander');
const inquirer = require('inquirer');
const fs = require('fs');
const path = require('path');
commander.command('showFiles [env]').action((option) => {
    // 判断存不存在
    // 判断是不是文件夹
    readFile(__dirname, option)
})
function readFile(dirname, name) {
    const url = path.join(dirname,name)
    const bool = fs.existsSync(url)
    if(bool) {
        // 判断是文件还是文件夹
        const stat = fs.statSync(url)
        // 判断是不是一个文件
        if(stat.isFile()) {
            // app.js
            console.log(`${name.split('.')[0]}---${name.split('.')[1]}---${stat.size}字节`)
        } else {
            // 获取文件夹的目录
            const files = fs.readdirSync(url)
            files.forEach(item => {
                readFile(url,item)
            })
        }
    } else {
        console.log('路径不存在')
    }
}
commander.parse(process.argv)