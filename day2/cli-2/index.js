#!/usr/bin/env node
const commander = require('commander')
const inquirer = require('inquirer')
const fs = require('fs')
const action = [
    {
        type:"input",
        message:"请输入名称",
        name: "name"
    },
    {
        type:"input",
        message:"你多大",
        name: "age"
    },
    {
        type:"input",
        message:"phone",
        name: "phone"
    },
    {
        type:"confirm",
        message:"是否继续录入",
        name: "bool"
    }
]
const list = []
commander.command('input').action(()=>{
    inputUserData()
})
async function inputUserData(){
    const res = await inquirer.prompt(action)
    list.push(res)
    if(res.bool){
        inputUserData()
    } else {
        fs.writeFileSync('userData.json', JSON.stringify(list))
    }
}
commander.parse(process.argv)
