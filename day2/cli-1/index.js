#!/usr/bin/env node
// 新建一个可执行文件 node格式
const commander = require('commander'); // 处理参数
const inquirer = require('inquirer'); // 处理用户交互
const fs = require('fs'); 
const initData = {
    name: "kkk",
    age:"18"
}
commander
    .command('init')
    .option('-y')
    .action((option)=>{
    console.log('init触发',option)
    if(option.y) {
        fs.writeFileSync('yourData.json', JSON.stringify(initData))
    } else {
        inquirer.prompt([
            {
                type:"input",
                message:"你叫啥",
                name: "name"
            },
            {
                type:"input",
                message:"你多打了",
                name: "age"
            }
        ]).then(res=>{
            fs.writeFileSync('yourData.json', JSON.stringify(res))
        })
    }
    
})

commander.parse(process.argv)