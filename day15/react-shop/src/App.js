import React from 'react';
import RouterView from './router';
import { routes } from './router/config';

const App = () => {
  return (
    <div className='App'>
      {/* 一级路由 */}
      <RouterView routes={routes}></RouterView>
    </div>
  );
}

export default App;
