import axios from '../utils/request'

export function getShopList(params){
    return axios.get('/list',{params})
}
export function getDetail(id){
    return axios.get('/list/'+id)
}
export function like(body){
    return axios.post('/like',body)
}
export function addToCart(body){
    return axios.post('/addToCart',body)
}
export function getCartList(params){
    return axios.get('/cart',{params})
}
export function delCart(id){
    return axios.delete('/cart/'+id)
}

export function login(body){
    return axios.post('/login',body)
}