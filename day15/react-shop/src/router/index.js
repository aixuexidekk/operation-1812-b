import React from 'react';
import {Switch, Route,Redirect} from 'react-router-dom'
const RouterView = ({routes}) => {
    // 路由列表  重定向列表
    const  routerList= routes.filter(item=>item.component)
    const redirectList= routes.filter(item=>item.to)
    return (
        <Switch>
            {routerList.map(item=>{
                return <Route key={item.path} path={item.path} render={(history)=>{
                    return <item.component {...history} routes={item.children || []}></item.component>
                }}></Route>
            })}
            {redirectList.map(item=>{
                return <Redirect {...item} key={item.to}></Redirect>
            })}
        </Switch>
    );
}

export default RouterView;
