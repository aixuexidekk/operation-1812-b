import Layout from '../layout'
import Home from '../pages/home'
import Cart from '../pages/cart'
import Detail from '../pages/detail'


export const routes = [
    {
        from:'/',
        to:'/layout'
    },
    {
        path:"/layout",
        component:Layout,
        children:[
            {
                from:'/layout',
                to:'/layout/home'
            },
            {
                path:"/layout/home",
                component:Home
            },
            {
                path:"/layout/cart",
                component:Cart
            }
        ]
    },
    {
        path:"/detail/:id",
        component:Detail
    }
]