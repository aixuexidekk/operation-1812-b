import React,{useEffect,useState} from 'react'
import { addToCart, getDetail } from '../../api';

export default function Detail(props) {
    const id = props.match.params.id;
    const [detail,setDetail] = useState(null)
    useEffect(() => {
        getDetail(id).then(res=>{
            if(res.code===1){
                setDetail(res.data)
            }
        })
    }, []);
    const addCart = (id)=>{
        addToCart({
            user_id:1,
            shop_id:id
        }).then(res=>{
            console.log(res)
        })
    }
    return (
        <>
            {detail?
                <div className="detail">
                    <img src={detail.image} alt="" />
                    {detail.title}
                </div>
                :<div></div>
            }
            <button onClick={() =>{addCart(detail.id)}}>加入购物车</button>
        </>
    )
}
