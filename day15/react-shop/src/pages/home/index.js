import React,{useState} from 'react';
import {Icon,Tabs,Carousel} from 'antd-mobile'
import './index.scss'
import List from '../../components/ListComponent'
const Home = (props) => {
    const [imgHeight,setImgHeight] = useState('176');
    const tabs = [
        { title: '黑丝' },
        { title: '渔网' },
        { title: '白丝' },
        { title: 'JK' },
        { title: '萝莉塔' },
        { title: '汉服' },
        { title: '皇帝新装' },
        { title: '兔女郎' },
    ];
  
    return (
        <div className="home">
            <div className="header">
                {/* Search */}
                <div className="search">
                    <div className="searchInput">
                        <Icon type="search" size="sm" />
                        请输入关键词
                    </div>
                </div>
                {/* Tab */}
                <Tabs tabs={tabs} renderTabBar={props => <Tabs.DefaultTabBar {...props} page={6} />}>
                </Tabs>
            </div>
            {/* Swiper */}
            <Carousel
                autoplay={false}
                infinite
                beforeChange={(from, to) => console.log(`slide from ${from} to ${to}`)}
                afterChange={index => console.log('slide to', index)}
                >
                {[
                        'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fci.xiaohongshu.com%2F09a62099-1fe3-4c28-b172-2a1259807dca%40r_640w_640h.jpg&refer=http%3A%2F%2Fci.xiaohongshu.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1627804651&t=4afe52aa62f5e2971a729657e3879136', 
                        'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fitem%2F202007%2F15%2F20200715175124_iljdb.thumb.1000_0.jpg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1627804651&t=0d572e3a01c25a9e1cec272d55f59536',
                        'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fitem%2F202004%2F19%2F20200419092654_suece.thumb.400_0.jpg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1627804651&t=dc481a4d844a73fc820407700855d982'].map(val => (
                    <a
                    key={val}
                    href="http://www.alipay.com"
                    style={{ display: 'inline-block', width: '100%', height: imgHeight }}
                    >
                    <img
                        src={val}
                        alt=""
                        style={{ width: '100%', verticalAlign: 'top' }}
                        onLoad={() => {
                        // fire window resize event to change height
                            window.dispatchEvent(new Event('resize'))
                            setImgHeight('auto')
                        }}
                    />
                    </a>
                ))}
                </Carousel>
            {/* Label */}
            {/* List */}
            <List {...props}></List>
        </div>
    );
}

export default Home;
