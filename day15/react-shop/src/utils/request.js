import axios from 'axios'
axios.interceptors.request.use(config=>{
    return {...config}
})
axios.interceptors.response.use(response=>{
    return response.data
},(err)=>{
    return Promise.reject(err)
})

export default axios;