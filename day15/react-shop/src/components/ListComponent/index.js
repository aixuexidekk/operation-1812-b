import React, {useState,useEffect} from 'react'
import { getShopList } from '../../api';
import "./index.scss"
export default function List(props) {
    const [pageIndex,setPageIndex] = useState(1);
    const [pageSize,setPageSize] = useState(10);
    const [total,setTotal] = useState(0);
    const [shopList,setShopList] = useState([]);
    const {keyword} = props;
    useEffect(() => {
        getShopList({
            pageIndex,
            pageSize,
            keyword
        }).then(res=>{
            if(res.code===1){
                setShopList(res.data.data)
                setTotal(res.data.total)
            }
        })
    }, [])
    const goDetail = (id) => {
        props.history.push('/detail/'+id)
    }
    return (
        <div className='list'>
            {shopList.map((item,index)=>{
                if((index+1)%2===0) return <></>
                return <div className="doubleBox">
                    <div className="box" onClick={()=>{goDetail(item.id)}}>
                        <img src={item.image} style={{width:"100%",height:"200px"}} alt="" />
                        {item.title}
                    </div>
                    {shopList[index+1]?<div className="box"  onClick={()=>{goDetail(shopList[index+1].id)}}>
                        <img src={shopList[index+1].image} style={{width:"100%",height:"200px"}} alt="" />
                        {shopList[index+1].title}
                    </div>:<div className='box'></div>}
                </div> 
            })}
        </div>
    )
}
