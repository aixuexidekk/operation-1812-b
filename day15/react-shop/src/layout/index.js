import React from 'react'
import { NavLink } from 'react-router-dom'
import RouterView from '../router'
import './index.scss'
export default function Layout(props) {
    return (
        <div className='layout'>
            {/* 视图 */}
            {/* 二级路由 */}
            <main>
                <RouterView routes={props.routes}></RouterView>
            </main>
            {/* 导航栏 */}
            <TabBar></TabBar>
        </div>
    )
}

function TabBar(){
    return <div className="tab_bar">
        <NavLink to="/layout/home">Home</NavLink>
        <NavLink to="/layout/cart">Cart</NavLink>
    </div>
}