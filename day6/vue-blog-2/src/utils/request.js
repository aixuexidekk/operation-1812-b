import axios from 'axios';
axios.defaults.baseURL = '/api'
// 请求头拦截器
axios.interceptors.request.use((config) => {
    // 本地存储中取出token
    const token = window.localStorage.getItem('token')
    // 给接口的请求头加入token  headers.Authorization是固定形式
    // 将token添加到请求头参数上
    token?config.headers.Authorization ="Bearer "+token:''
    return config;
})
// 响应头拦截器
axios.interceptors.response.use((response) => {
    return response.data
},err=> {
    switch(err.response.status) {
        case 401: 
        alert('你没权限')
    }
    return Promise.reject(err)
})

export default axios;

