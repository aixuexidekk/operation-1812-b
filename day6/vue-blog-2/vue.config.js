/*
 * @Author: your name
 * @Date: 2021-06-22 19:54:20
 * @LastEditTime: 2021-06-22 20:44:44
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-blog-2\vue.config.js
 */
module.exports = {
    devServer: {
        proxy: {
            '/api': {
                target: "http://localhost:8888",
                pathRewrite: {
                    '^/api': ""
                }
            }
        }
    }
}