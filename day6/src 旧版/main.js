import Vue from 'vue'
import App from './App.vue'
import api from './api'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);
Vue.prototype.$api =api;
Vue.config.productionTip = false
// const div = document.createElement('div')
// Vue.directive('loading2', {
//   bind(el,data){
//     div.innerHTML='loading'
//     div.id='loading'
//     div.style.display='none'
//     el.appendChild(div)
//   },
//   update(el,data){
//     if(data.value) {
//       div.style.display='block'
//     }else{
//       div.style.display='none'
//     }
//   }
// })
new Vue({
  render: h => h(App),
}).$mount('#app')
