import axios from 'axios';
axios.defaults.baseURL = '/api'
// 请求头拦截器
axios.interceptors.request.use((config) => {
    return config;
})
// 响应头拦截器
axios.interceptors.response.use((response) => {
    return response.data
},err=> {
    return Promise.reject(err)
})

export default axios;

