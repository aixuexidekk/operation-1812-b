import axios from '../../utils/request'

// 获取列表
function getList(query){
    return axios.get('/blog/list', {params: query})
}
// 新增列表
function addList(body){
    return axios.post('/blog/list', body)
}
// 编辑列表
function editList(body){
    return axios.put('/blog/list', body)
}
// 删除列表
function delList(id){
    return axios.delete(`/blog/list/${id}`)
}

export default {
    getList,
    addList,
    editList,
    delList,
}

