module.exports = app => {
    const { router, controller, middleware } = app;
    // 获取博客列表
    router.get('/list', controller.list.getList);
    // 添加博客列表
    router.post('/list', controller.list.addList);
    // 编辑博客列表
    router.put('/list', controller.list.editList);
    // 删除博客
    router.delete('/list/:id', controller.list.delList);
    // // 登录
    // router.post('/login', controller.user.login);
    // // 注册
    // router.post('/register', controller.user.register);
};