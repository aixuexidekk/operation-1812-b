const Controller = require('egg').Controller;

class ListController extends Controller {
    async getList() {
        const {ctx,app} = this; // ctx 上下文  app 跟实例  有且只有一个
        const data = await ctx.service.list.getList()
        ctx.body= data;
    }
    async addList() {

    }
    async editList() {

    }
    async delList() {

    }
}

module.exports = ListController;
