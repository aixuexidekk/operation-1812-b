const moment = require('moment');
// 时间戳转为相对时间
// 昨天 前台 一周前
exports.relativeTime = time => moment(new Date(time)).fromNow();
exports.double = num => num*2
