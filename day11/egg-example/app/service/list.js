const Service = require('egg').Service;

class ListService extends Service {
  async getList() {
    const { ctx, app } = this; // ctx 上下文  app 跟实例  有且只有一个
    // 获取数据库数据
    const data = await app.mysql.query('select * from blogs')
    return data;
  }
  async addList() {

  }
  async editList() {

  }
  async delList() {

  }
}

module.exports = ListService;
