import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Bus from './plugin/bus'
Vue.use(ElementUI); // ElementUi.install(Vue)
Vue.use(Bus) // Bus.install(Vue)
Vue.config.productionTip = false
new Vue({
  render: h => h(App)
}).$mount('#app')

//