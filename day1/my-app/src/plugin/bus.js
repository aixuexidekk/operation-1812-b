
class Bus{
    constructor() {
      this.eventList = []
    }
    // this.$bus.$on('hbb', ()=>{}) => {hbb: ()=>{}}
    $on(eventName, callBack) {
      this.eventList.push({
        [eventName]: callBack
      })
    }
    // this.$bus.$emit('hbb','bb')
    $emit(eventName, data) {
      const callback = this.eventList.find(item => item[eventName])
      if(callback) { // {hbb: ()=>{}}
        callback[eventName](data)
      }
    }
    // this.$bus.$off('hbb')
    $off(eventName) {
      const ind = this.eventList.findIndex(item => item[eventName])
      this.eventList.splice(ind,1)
    }
}
export default {
    install(Vue) {
        Vue.prototype.$bus = new Bus(); // $on $emit $off
    }
}