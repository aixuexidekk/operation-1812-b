/*
 * @Author: your name
 * @Date: 2021-06-28 08:50:11
 * @LastEditTime: 2021-06-28 10:47:01
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-app\src\store\index.js
 */
import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios"
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    getlist: []
  },
  mutations: {
    async GET_DEL(state, payload) {
      await axios.delete("/api/Del", { params: { id: payload } })
    },
    async GET_ADD(state, payload) {
      console.log(payload);
      await axios.get("/api/add", {
        params: {
          id:payload,
          name:123,
        }
      })
    }
  },
  actions: {
    async get_list() {
      const res = await axios.get("/api/data")
      this.state.getlist = res.data.data
      console.log(res.data.data);
    }
  },
  modules: {
  }
})
