/*
 * @Author: your name
 * @Date: 2021-06-28 08:37:04
 * @LastEditTime: 2021-06-28 10:54:23
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \6.28月考\koaapp\routes\index.js
 */
const router = require('koa-router')()
const { exec } = require("../db/mysql")
router.get('/', async (ctx, next) => {
  await ctx.render('index', {
    title: 'Hello Koa 2!'
  })
})

router.get("/api/data", async (ctx, next) => {
  const sql = "select * from shopa"
  const data = await exec(sql)
  ctx.body = { data }
})

router.delete("/api/Del", async (ctx, next) => {
  const { id } = ctx.query
  const sql = `delete from shopa where id=${id}`
  const data = await exec(sql)
  ctx.body = { data }
})

router.get("/api/add", async (ctx, next) => {
  const { id, name} = ctx.query
  console.log(name);
  const sql = `update shopa set name = ${name},book= ${name},title=${name},content=${name} WHERE id = ${id}`
  const data = await exec(sql)
  ctx.body = { data }
})


router.get("/api/login", async (ctx, next) => {
  const {user} = ctx.query
  console.log(name);
  const sql = `SELECT * FROM users WHERE user = ${user} LIMIT 0, 1;`
  const data = await exec(sql)
  ctx.body = { data }
})
module.exports = router
