/*
 * @Author: your name
 * @Date: 2021-06-28 08:41:38
 * @LastEditTime: 2021-06-28 08:46:06
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \6.28月考\koaapp\db\mysql.js
 */
//下载npm i mysql -S
const mysql = require('mysql')
//链接数据库
const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'shop'
})

function exec(sql) {
    return new Promise((resolve, reject)=>{
        conn.query(sql,(err,data)=>{
            if(err){
                reject(err)
            }else{
                resolve(data)
            }
        })
    })
}
module.exports={exec}
