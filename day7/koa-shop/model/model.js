// 父类
class Model {
    constructor(data,message) {
        if(data) this.data =data;
        if(message) this.message =message;
    }
}
// 成功类
class SuccessModel extends Model {
    constructor(data,message) {
        super(data,message)
        this.code=1
    }
}
// 失败类
class ErrorModel extends Model {
    constructor(message) {
        super(null, message)
        this.code=-1
    }
}
function success(data,message) {
    return new SuccessModel(data,message)
}
function error(message) {
    return new ErrorModel(message)
}
module.exports = {
    success,
    error
}
