const query = require('../db/mysql')
class Home {
    // 获取商品列表 ： 分页 模糊查询 排序 
    // ?id=1
    getShopList = async (ctx) => {
        const {pageIndex,pageSize,keyword,sortBy,sortType} = ctx.query
        let sql = `select * from shops where 1=1 `
        let totalSql = `select count(id) as total from shops where 1=1 `
        if(keyword) {
            sql += `and title like "%${keyword}%" `
            totalSql += `and title like "%${keyword}%" `
        }
        if(sortBy && sortType) {
            sql += `order by ${sortBy} ${sortType} `
        }
        if(pageIndex && pageSize) {
            sql += `limit ${(pageIndex-1)*pageSize},${pageSize} `
        }
        const data = await query(sql)
        const total = await query(totalSql)
        ctx.body ={
            code:1,
            data:{
                data,
                total: total[0].total
            }
        }
    }
    getShopDetail = async(ctx) => {
        const {id} =ctx.params;
        const sql = `select * from shops where id="${id}"`
        const data = await query(sql)
        ctx.body = {
            code:1,
            data
        }
    }
}

module.exports = new Home()