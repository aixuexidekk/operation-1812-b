const jwt = require('jsonwebtoken')
async function auth(ctx,next) {
    const {authorization} = ctx.request.header;
    if(authorization) {
        const token = authorization.split(' ')[1]
        try{
            jwt.verify(token, '1812b')
            await next()
        } catch(err) {
            ctx.status = 401; // 无权
            ctx.body = {
                code: -1,
                message:"鉴权失败" + err.message
            }
        }
    } else {
        ctx.status = 401; // 无权
        ctx.body = {
            code: -1,
            message:"鉴权失败"
        }
    }
}
module.exports = auth