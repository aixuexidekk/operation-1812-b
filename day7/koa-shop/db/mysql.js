// database 数据库
const mysql = require('mysql')
const con = mysql.createConnection({
    host:"localhost",
    port:"3306",
    user:"root",
    password:"123456",
    database: "shop-12b"
})
con.connect()
function query(sql) {
    return new Promise((resolve,reject) => {
        con.query(sql, (err,data) => {
            if(err) reject(err)
            if(data) resolve (data)
        })
    })
}

module.exports = query;