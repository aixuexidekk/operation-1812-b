const mock = require('mockjs')
const query =require('./mysql')
const {list} = mock.mock({
    'list|300': [
        {
            title: "@ctitle(5,10)",
            content: "@ctitle(10,20)",
            image: "https://unsplash.it/400/800/?@name",
            "price|1-1000":1,
            "sales|1-1000":1,
            "stock|1-1000":1,
        }
    ]
})

list.forEach((item) => {
    let sql = `insert into shops(title,content,image,price,sales,stock) values("${item.title}","${item.content}","${item.image}","${item.price}","${item.sales}","${item.stock}")`
    query(sql)
})