/*
 * @Author: your name
 * @Date: 2021-06-24 09:39:34
 * @LastEditTime: 2021-06-25 13:41:49
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \代码git\operation-1812-b\day7\koa-shop\index.js
 */
const Koa = require('koa')
const Cors = require('koa-cors')
const bodyParser = require('koa-bodyparser')
const router = require('./router')
const app =new Koa()

// app.use(Cors())
app.use(bodyParser())
router(app)// 挂载路由
app.listen(8899,() => {
    console.log('listen to 8899')
})

/*
场景：客厅

人物两人：A B

A在沙发上浏览网页，起身对B说：
一母亲在网上征求给宝宝起名生
宝宝时孕妇在医院刚生完宝宝医
院着了大火，一场大雨浇灭了大
火，网友纷纷发表意见，叫“淡”
因为水熄灭了火！
A说：万一他老公姓“姬”
B说：万一姓“曹”呢！
*/