const router = require('koa-router')();
const {getShopList,getShopDetail} = require('../controller/home'); // 抽离复杂业务逻辑代码
router.get('/shop/list',getShopList)
router.get('/shop/detail/:id',getShopDetail)
module.exports = router;