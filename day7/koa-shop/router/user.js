const router = require('koa-router')();
const {login,register} = require('../controller/user.js')
router.post('/login', login)
router.post('/register', register)
module.exports = router;
