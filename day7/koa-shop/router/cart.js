const router = require('koa-router')();
const {getCartList,addToCart,delCart,setCartCount} = require('../controller/cart')
const auth = require('../middleware/auth.js')
router.get('/cart/list', auth, getCartList)
router.post('/cart/add',  auth, addToCart)
router.delete('/cart/delete/:id',  auth, delCart)
router.put('/cart/setcount',  auth, setCartCount)
module.exports = router;
