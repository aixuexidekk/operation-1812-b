module.exports = () => {
    return async function auth(ctx,next) {
        const {authorization} = ctx.request.header;
        // 如果传了则去校验
        if(authorization){
            const token = authorization.split(' ')[1]
            const jwt = require('jsonwebtoken')
            try{
                jwt.verify(token, ctx.app.config.keys);
                await next()
            }catch(err){
                ctx.status = 401;
                ctx.body = {
                    code:-1,
                    message:'你没有权限'+err.message
                }
            }
        }else{
            ctx.status = 401;
            ctx.body = {
                code:-1,
                message:'你没有权限'
            }
        }
    }
}