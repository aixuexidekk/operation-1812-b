const Controller = require('egg').Controller;

class BlogController extends Controller {
    async getList() {
        // controller只负责收发数据
        // service 负责复杂业务逻辑
        const {ctx,app} = this;
        const data = await ctx.service.blog.getList()
        if(typeof data === 'string') {
            ctx.body= {
                code:-1,
                message:data
            }
        } else {
            ctx.body = {
                data,
                code:1
            }
        }
    }
    async addList() {
        const {ctx,app} = this;
        const data = await ctx.service.blog.addList()
        if(data.affectedRows === 1) {
            ctx.body= {
                code:1,
                message:'添加成功'
            }
        } else {
            ctx.body = {
                message:'添加失败',
                code:-1
            }
        }
    }
    async editList() {
        const {ctx,app} = this;
        const data = await ctx.service.blog.editList()
        if(data.affectedRows === 1) {
            ctx.body= {
                code:1,
                message:'更新成功'
            }
        } else {
            ctx.body = {
                message:'更新失败',
                code:-1
            }
        }
    }
    async delList() {
        const {ctx,app} = this;
        const data = await ctx.service.blog.delList()
        if(data.affectedRows === 1) {
            ctx.body= {
                code:1,
                message:'删除成功'
            }
        } else {
            ctx.body = {
                message:'删除失败',
                code:-1
            }
        }
    }
}

module.exports = BlogController;
