const Controller = require('egg').Controller;

class UserController extends Controller {
    async login() {
        const {ctx,app} = this;
        const data = await ctx.service.user.login()
        if(typeof data =='string'){
            ctx.body = {
                code:-1,
                message:data
            }
        } else {
            ctx.body ={
                code:1,
                data
            }
        }
    }
    async register() {
        const {ctx,app} = this;
        const data = await ctx.service.user.register()
        if(typeof data =='string'){
            ctx.body = {
                code:-1,
                message:data
            }
        } else {
            ctx.body ={
                code:1,
                message:"注册成功"
            }
        }
    }
}

module.exports = UserController;
