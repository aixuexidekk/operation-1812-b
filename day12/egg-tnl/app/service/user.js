const Service = require('egg').Service;
const jwt = require('jsonwebtoken')
const {md5} = require('utility')
class UserService extends Service {
    async login() {
        const {ctx,app} = this;
        const {username,password} = ctx.request.body;
        const isHave = await app.mysql.get('users', {username, password:md5(String(password))})
        if(isHave){
            return {
                data:isHave,
                token: jwt.sign({...isHave}, app.config.keys)
            }
        } else {
            return '请检查账号密码'
        }
    }
    async register() {
        const {ctx,app} = this;
        const {username,password,nickname} = ctx.request.body;
        const isHave = await app.mysql.get('users', {username})
        if(isHave) {
            return '账号已存在'
        }
        const data = await app.mysql.insert('users', {...ctx.request.body, password: md5(String(password))})
        return data
    }
}

module.exports = UserService;
