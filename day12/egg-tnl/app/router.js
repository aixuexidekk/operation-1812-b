module.exports = app =>{
    const {router,controller} = app
    const {auth} =app.middleware;
    router.get('/list', controller.blog.getList); // 接口=>控制器=>service=>返数据 给控制器=>ctx.body=数据
    router.post('/list',auth(), controller.blog.addList);
    router.put('/list', auth(),controller.blog.editList);
    router.delete('/list/:id',auth(), controller.blog.delList);

    router.post('/login', controller.user.login)
    router.post('/register', controller.user.register)
}