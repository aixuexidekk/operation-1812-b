import React, {useState} from 'react';
import { Modal, Button, Form,Input } from 'antd'
const { TextArea } = Input;
const FormItem = Form.Item;
const formLayout = {
    labelCol: {
        span: 7,
    },
    wrapperCol: {
        span: 13,
    },
};
const UpdateForm = (props) => {
     // 默认参数
    const [formVals, setFormVals] = useState({
        title: props.values.title,
        content: props.values.content,
    });
    const {
        updateModalVisible,
        onCancel: handleUpdateModalVisible,
        onSubmit,
        values,
    } = props;
    const [form] = Form.useForm();

    const renderFooter = () => {
        return <>
            <Button type="primary" onClick={async () => {
                // 校验
                 const fieldsValue = await form.validateFields();
                 console.log(fieldsValue)
                 // 修改表单默认参数
                 setFormVals((prev) => {
                     const newValue = { ...formVals, ...fieldsValue,id:values.id }
                    onSubmit(newValue)
                     return newValue
                 });
            }}>
                提交
            </Button>
        </>
    }
    return (
        <Modal
            width={640}
            bodyStyle={{
                padding: '32px 40px 48px',
            }}
            destroyOnClose
            title="规则配置"
            visible={updateModalVisible}
            footer={renderFooter()}
            onCancel={() => handleUpdateModalVisible()}
        >
            <Form
                {...formLayout}
                form={form}
                initialValues={{
                    title:formVals.title,
                    content:formVals.content
                }}
            >
                <FormItem
                    name="title"
                    label="博客名称"
                    rules={[
                        {
                            required: true,
                            message: '请输入博客名称',
                        },
                    ]}
                >
                    <Input placeholder="请输入" />
                </FormItem>
                <FormItem
                    name="content"
                    label="博客内容"
                    rules={[
                        {
                            required: true,
                            message: '至少输入20个字',
                            min: 20,
                        },
                    ]}
                >
                    <TextArea rows={4} placeholder="请至少输入20个字" />
                </FormItem>
            </Form>
        </Modal>
    );
}

export default UpdateForm;
