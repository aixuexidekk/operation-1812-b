import React from 'react';
import { Modal } from 'antd';
const CreateForm = (props) => {
    const {onCancel, modalVisible} = props;
    return (
        <Modal
            destroyOnClose
            title="新建规则"
            visible={modalVisible}
            onCancel={() => onCancel()}
            footer={null}
            >
                {/* 向vue里的插槽 */}
            {props.children}
        </Modal>
    );
}

export default CreateForm;
