import request from 'umi-request';
export async function getList(params) {
  return request('/kkk/list', {
    params,
  });
}
export async function delList(params) {
  return request('/kkk/list', {
    method: 'POST',
    data: { ...params},
  });
}
export async function addList(params) {
  return request('/kkk/list', {
    method: 'POST',
    data: { ...params },
  });
}
export async function editList(params) {
  return request('/kkk/list', {
    method: 'put',
    data: { ...params },
  });
}
