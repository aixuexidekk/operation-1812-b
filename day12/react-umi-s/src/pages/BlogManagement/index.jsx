import React,{useState,useRef} from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import { Button, Divider, message, Input, Drawer } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { getList, addList, editList, delList } from './service'
import CreateForm from './components/CreateForm'
import UpdateForm from './components/UpdateForm'

const Blog = () => {
    const [createModalVisible, handleModalVisible] = useState(false);
    const [updateModalVisible, handleUpdateModalVisible] = useState(false);
    const [stepFormValues, setStepFormValues] = useState(null);

    const actionRef = useRef();
    const columns = [
        {
            title: '博客名称',
            dataIndex: 'title',
            tip: '这是博客的标题',
            formItemProps: {
                rules: [
                    {
                        required: true,
                        message: '标题必填',
                    },
                ],
            },
        },
        {
            title: '博客作者',
            dataIndex: 'author',
            tip: '作者'
        },
        {
            title: '内容',
            dataIndex: 'content',
            tip: '内容！'
        },
        {
            title: '创建时间',
            dataIndex: 'create_time',
            hideInForm: true,
            sorter: true,
            render: (dom, entity) => {
                // toLocaleDateString 不建议用 坑多
                // 建议使用dayjs 或者monentjs
                return new Date(dom).toLocaleDateString()
            }
        },
        {
            title: "操作",
            render: (dom,record) => {
                return <Button onClick={() => {
                    handleUpdateModalVisible(true)
                    console.log(record)
                    setStepFormValues(record); // 回显数据
                }}>编辑</Button>
            }
        }
    ];
    return (
        <PageContainer>

            <CreateForm onCancel={() => {handleModalVisible(false)}} modalVisible={createModalVisible}>
                <ProTable
                    onSubmit={async (value) => {
                        const success = await addList(value); // 掉接口
                        if (success.code===1) {
                            handleModalVisible(false); // 关闭弹框
            
                            if (actionRef.current) {
                                actionRef.current.reload(); // 刷新一遍列表数据
                            }
                        }
                    }}
                    rowKey="key"
                    type="form"
                    columns={columns}
                />
            </CreateForm>
            {stepFormValues ?<UpdateForm 
                onSubmit={async (value)=>{
                    const data = await editList(value)
                    if(data.code===1){
                        handleUpdateModalVisible(false)// 关闭更新弹框
                        setStepFormValues(null);  // 让组件销毁 从而重新渲染
                        if (actionRef.current) {
                            actionRef.current.reload(); // 刷新一遍列表数据
                        }
                    }
                }}
                onCancel={() => {
                    handleUpdateModalVisible(false)
                    setStepFormValues(null);  // 让组件销毁 从而重新渲染
                }} 
                updateModalVisible={updateModalVisible}
                values={stepFormValues}
            />:''}
            <ProTable
                headerTitle="查询表格"
                actionRef={actionRef}
                rowKey="key"
                search={{
                    labelWidth: 120,
                }}
                toolBarRender={() => [
                    <Button type="primary"  onClick={()=>{handleModalVisible(true)}}>
                        <PlusOutlined/> 新建
                    </Button>,
                ]}
                request={async (params, sorter, filter) => {
                    console.log(params, sorter, filter)
                    const data = await getList({
                        pageIndex: params.current,
                        pageSize: params.pageSize,
                        keyword: params.title,
                        sortBy: Object.keys(sorter)[0],//  {a:1,b:2} = [a,b]
                        sortType: sorter[Object.keys(sorter)[0]] ? (sorter[Object.keys(sorter)[0]] === 'descend' ? 'desc' : 'asc') : ''
                    })
                    return {
                        data: data.data.data.map(items => {
                            return { ...items, key: items.id }
                        }), // 数组必须有key这个属性
                        success: data.code === 1,
                        total: data.data.total,
                    }
                }}
                columns={columns}
            />
        </PageContainer>
    );
}

export default Blog;
