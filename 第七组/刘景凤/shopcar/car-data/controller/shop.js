const query = require("../db/mysql");
const { sucess,error } = require("../modle/modle");

class Shop {
    getList = async (ctx) => {   //获取商品数据 模糊查询 上拉加载 排序
        const { keyword, sortBy, sortType, pageSize, pageIndex } = ctx.query;
        let sql = `select * from shop where 1=1 `;
        let sqlcount=`select count(id) as total from shop where 1=1 `;
        if (keyword) {
            sql+=`and title like '%${keyword}%' `;
            sqlcount+=`and title like '%${keyword}%' `;
        }
        if(sortBy&&sortType){
            sql+=`order by '${sortBy}' '${sortType}' `;
        }
        if(pageIndex&&pageSize){
            sql+=`limit ${(pageIndex-1)*pageSize},${pageSize}`
        }
        const data=await query(sql)
        const total=await query(sqlcount)
        // console.log(data,total)
        if(data.length){
            ctx.body=sucess({
                total:total[0].total,
                data:data,
            },'获取成功')
        }else{
            ctx.body=error('获取商品数据失败')
        }
    }
    getShopDetail=async(ctx)=>{
        const {id}=ctx.params;
        // console.log(id)
        let sql=`select * from shop where id='${id}'`
        const data=await query(sql)
        if(data.length){
            ctx.body=sucess({
                data
            },'获取详情')
        }else{
            ctx.body=error('获取详情失败')
        }
    }
}

module.exports = new Shop()