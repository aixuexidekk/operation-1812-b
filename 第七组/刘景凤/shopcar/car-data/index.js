const Koa=require('koa');
const app=new Koa();
const bodyParser = require('koa-bodyparser');
const router = require('./router');


app.use(bodyParser())
router(app)  //挂载路由
app.listen(8888,()=>{
    console.log('listen to 8888')
})