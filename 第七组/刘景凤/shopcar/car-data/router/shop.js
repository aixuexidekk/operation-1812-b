const Route=require('koa-router');
const router=new Route();
const {getList,getShopDetail}=require('../controller/shop')

//获取商品数据接口
router.get('/api/shop',getList)
//获取详情接口
router.get('/api/detail/:id',getShopDetail)



module.exports=router