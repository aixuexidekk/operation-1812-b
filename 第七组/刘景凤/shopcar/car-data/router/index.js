const fs=require('fs')
const files=fs.readdirSync(__dirname)

module.exports=(app)=>{
    files.forEach(item=>{
        if(item==='index.js') return ;
        const router=require(`./${item}`)   //将所有目录router引入此页面
        app.use(router.routes())
    })
}