import Vue from 'vue'

import { Card,GoodsAction, GoodsActionIcon,Button,GoodsActionButton,SubmitBar,Checkbox,SwipeCell} from 'vant';

Vue.use(Card);
Vue.use(GoodsAction);
Vue.use(GoodsActionButton);
Vue.use(GoodsActionIcon);
Vue.use(SubmitBar);
Vue.use(Checkbox);
Vue.use(SwipeCell);
Vue.use(Button);