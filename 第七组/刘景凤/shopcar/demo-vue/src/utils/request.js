import axios from 'axios'

axios.interceptors.request.use((config)=>{
    const token=window.localStorage.getItem('token')
    token?config.headers.authorization='Bearer'+token:'';
    return config
})

axios.interceptors.response.use((response)=>{
    return response.data
})

export default axios