import user from './user'
import shop from './shop'
import cart from './cart'

export default {
    ...user,
    ...shop,
    ...cart
}