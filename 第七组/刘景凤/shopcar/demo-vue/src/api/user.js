import axios from '@/utils/request.js'

function getLogin(body){
   return  axios.post('/api/login',body)
}
function getRegister(body){
   return axios.post('/api/register',body)
}

export default {
    getLogin,
    getRegister
}