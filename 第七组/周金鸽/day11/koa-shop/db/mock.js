// const mock=require("mockjs");
const mock=require("mockjs")
const query=require("./mysql");
const {list}=mock.mock({
    "list|20":[{
        title:"@ctitle(5,10)",
        count:"@ctitle(10,20)",
        image:"https://unsplash.it/400/800/?@name",
        "price|1-1000":1,
        "sales|1-1000":1,
        "stock|1-1000":1
    }]
})
list.forEach((item)=>{
    let sql=`insert into shop(title,count,image,price,sales,stock) value("${item.title}","${item.count}","${item.image}","${item.price}","${item.sales}","${item.stock}") `;
    query(sql);
})