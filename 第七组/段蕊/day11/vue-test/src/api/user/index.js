import axios from '../../utils/request'

///登录
function login(body){
    return axios.post('/login',body)
}
//注册
function register(body){
    return axios.post('/register',body)
}

export default{
    login,
    register
}