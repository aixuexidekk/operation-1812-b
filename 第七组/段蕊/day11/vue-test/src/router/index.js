import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
  path: '/',
  redirect: '/home'
},
{
  path: '/home',
  component: () => import(/* webpackChunkName: "home" */ '../views/home'),
  children:[{
    path:'/home',
    redirect:'/home/list'
  },{
    path:'/home/list',
    component: () => import(/* webpackChunkName: "list" */ '../views/list'),
  },{
    path:'/home/cart',
    component: () => import(/* webpackChunkName: "Cart" */ '../views/cart'),
  },{
    path:'/home/about',
    component: () => import(/* webpackChunkName: "About" */ '../views/about'),
  }]
},{
  path:'/login',
  component: () => import(/* webpackChunkName: "login" */ '../views/login'),
},{
  path:'/detail/:id',
  component: () => import(/* webpackChunkName: "detail" */ '../views/detail'),
}]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
