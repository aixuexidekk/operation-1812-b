class Midel{
   constructor(data,message){
       if(typeof data==='string'){
           this.message=data
           return
       }
       if(data)this.data=data;
       if(message) this.message=message
   }
}

class SuccessMidel extends Midel{
    constructor(data,message){
        super(data,message)
        this.code=1
    }
}

class ErrorMidel extends Midel{
    constructor(data,message){
        super(data,message)
        this.code=-1
    }
}

function success(data,message){
    return new SuccessMidel(data,message)
}

function error(data,message){
    return new ErrorMidel(data,message)
}
module.exports={
    success,
    error
}