const mock=require('mockjs')
const query=require('./mysql')

const {list}=mock.mock({
    'list|400':[{
        'title':'@ctitle(5,10)',
        'content':'@ctitle',
        'price|1-1000':1,
        'sales|1-1000':1,
        'stock|1-1000':1,
        'image':'https://unsplash.it/400/800/?@name'
    }]
})

list.forEach(item=>{
    let sql=`insert into shops (title,content,price,sales,stock,image) values("${item.title}","${item.content}","${item.price}","${item.sales}","${item.stock}","${item.image}") `;
    query(sql)
})