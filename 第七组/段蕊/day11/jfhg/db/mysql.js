const mysql=require('mysql')

const con=mysql.createConnection({
    port:'3306',
    host:'localhost',
    user:'root',
    password:'root',
    database:'shopping'
})
///开启链接
con.connect()

function query(sql){
    return new Promise((resolve,reject)=>{
        con.query(sql,(err,data)=>{
            if(err){
                reject(err)
            }else{
                resolve(data)
            }
        })
    })
}

module.exports=query