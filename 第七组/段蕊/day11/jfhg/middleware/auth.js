const jwt=require('jsonwebtoken')

const auth=async(ctx,next)=>{
    const {authorization}=ctx.request.header;
    if(authorization){
        const token=authorization.split(' ')[1];
        try{
            jwt.verify(token,'1812b')
            await next()
        }catch(error){
            ctx.status=401;
            ctx.body={
                code:-1,
                message:'鉴权失败'+error.message
            }
        }
    }else{
        ctx.body={
            code:-1,
            message:'无权限'
        }
    }
}

module.exports=auth