const query=require('../db/mysql')
class Home{
    ///商品列表数据
    getShopList=async(ctx)=>{
        const {id,keyword,pageIndex,pageSize,sortBy,sortType}=ctx.query;
        let sql=`select * from shops where 1=1 `
        let totalSql=`select count(id) as count from shops where 1=1 `
        if(id){
            sql +=`and id=${id} `
        }
        if(keyword){
            sql += `and title like "%${keyword}%" `
            totalSql+=`and title like "%${keyword}%" `
        }
        if(pageIndex&pageSize){
            sql+=`limit ${(pageIndex-1)*pageSize},${pageSize} `
        }
        if(sortBy&sortType){
            sql+=`order by ${sortBy} ${sortType} `
        }
        const data=await query(sql)
        const total=await query(totalSql)
        ctx.body={
            code:1,
            data:{
                data,
                total:total[0].count
            }
        }
    }
    ///商品详情数据
    getDetailShop=async(ctx)=>{
        const {id}=ctx.params;
        const sql=`select * from shops where id="${id}" `;
        const data=await query(sql)
        ctx.body={
            code:1,
            data
        }
    }
 }
 module.exports=new Home()