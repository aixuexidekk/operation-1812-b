const fs=require('fs')

const files=fs.readdirSync(__dirname);
module.exports=(app)=>{
    files.forEach(item=>{
        if(item==='index.js') return
        const router=require(`./${item}`);
        app.use(router.routes())
    })
}