const Router=require('koa-router')
const router=new Router()
const {getShopList,getDetailShop}=require('../controller/home')

router.get('/shop/list',getShopList)
router.get('/shop/detail/:id',getDetailShop)

module.exports=router