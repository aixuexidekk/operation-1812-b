const Router=require('koa-router')
const router=new Router()
const {getCartList,addCartList,delCartList} =require('../controller/cart')

router.get('/cart/list',getCartList)
router.post('/cart/add',addCartList)
router.delete('/cart/del/:id',delCartList)

module.exports=router