module.exports = {
    devServer: {
        // proxy: "http://localhost:8888"
        proxy:  {
            '/list': {     //这里最好有一个 /
                target: 'http://localhost:8888',  // 后台接口域名
                pathRewrite:{
                    '^/list':'/list'
                }
            },
            '/api2': {     //这里最好有一个 /
                target: 'http://localhost:9999',  // 后台接口域名
                pathRewrite:{
                    '^/api2':''
                }
            }
        }
    }
}
