import axios from '../../utils/request'

function getList(query) {
    // /list?pageIndex=1&pageSize=10
    return axios.get('/list', {params: query})
}
function addList(body) {
    return axios.post('/list', body)
}
function editList(body) {
    return axios.put('/list', body)
}
function delList(id) {
    return axios.delete(`/list/${id}`)
}


export default {
    getList,
    addList,
    editList,
    delList
}