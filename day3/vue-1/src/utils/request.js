import axios from 'axios';
// 请求头拦截器
axios.interceptors.request.use((config) => {
    // 可以对请求头的header做一些配置 比如添加一个token
    const token = window.localStorage.getItem('token')
    const headers = {}
    if(token) {
        headers['Authorization'] = `Bearer ${token}`
    }
    return {
        ...config,
        headers
    }
})

// 响应头拦截器
axios.interceptors.response.use((response) => {
    return response.data
}, error=> {
    // 判断状态码
    switch(error.response.status) {
        case "401": 
        console.log('无权限')
        break;
        case "403": 
        console.log('访问受限')
        break;
        case "404": 
        console.log('接口未找到')
        break;
        case "500": 
        console.log('服务器错误')
        break;
    }
    return Promise.reject(error)
})

export default axios;