const Koa = require('koa')
const app = new Koa()

// ctx =Context 上下文
// request response
// ctx = {request, response}
// 1. 洋葱圈运行机制  从外向里 再从里向外
// 2. ctx 是通用的 会向后传递
// 3. 每一个中间件必须是async函数
// localhost:8888/login
// localhost:8888/home
app.use(async (ctx,next) => {
    const startTime=Date.now()
    console.log(1,ctx.id)
    await next()
    const endTime=Date.now()
    console.log(2,ctx.id)
})
app.use(async (ctx,next) => {
    console.log(3,ctx.id)
    await new Promise((res) => {
        setTimeout(() => {
            ctx.id = 9999;
            res()
        }, 800);
    })
    await next()
    console.log(4,ctx.id )
})
app.use(async (ctx,next) => {
    console.log(5,ctx.id)
})


app.listen(8888, () => {
    console.log('listen to 8888')
})