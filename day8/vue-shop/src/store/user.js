
import api from '../api'
export default {
    namespaced: true, // 开启命名空间
    state: {
        userData: {}
    },
    mutations: {
        setUserData(state,data) {
            state.userData = data
        } 
    },
    actions: {
        LOGIN(context,data) {
            return new Promise((resolve,reject) => {
                // 登录接口
                api.login(data).then(res=>{
                    if(res.code===1) {
                        // 存储到vuex的state
                        context.commit('setUserData', res.data)
                        // 返回成功
                        resolve(res)
                    } else {
                        reject(res)
                    }
                }).catch(err=>{
                    reject(err)
                })
            })
        }
    }
  }
  