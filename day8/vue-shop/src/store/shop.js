
import api from '../api'
export default {
    namespaced: true, // 开启命名空间
    state: {
        shopList:[],
        total: 0
    },
    mutations: {
        setShopList(state, data) {
            state.shopList = state.shopList.concat(data);
        },
        setTotal(state,total){
            state.total = total
        },
        clearShopList(state) {
            state.shopList= []
        }
    },
    actions: {
        SET_SHOP_LIST(context, data) {
           return new Promise((resolve,reject) => {
                api.getShopList(data).then(res=>{
                    if(res.code===1) {
                        context.commit('setShopList', res.data.data)
                        context.commit('setTotal', res.data.total)
                    }
                    resolve(res)
                }).catch(err=>{
                    reject(err)
                })
           })
        }
    }
  }
  