import Vue from 'vue'
import Vuex from 'vuex'
import shop from './shop'
import user from './user'
import persistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [persistedState({ storage: window.localStorage })], // vuex长效存储  把vuex所有state存储到本地存储
  modules: {
    shop,
    user
  }
})
