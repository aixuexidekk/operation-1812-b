import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '../layout'
Vue.use(VueRouter)

const routes = [
  {
    path:"/",
    redirect:"/layout"
  },
  {
    path:'/layout',
    component: Layout,
    children: [
      {
        path:'/',
        redirect:"/layout/home"
      },
      {
        path: "home",
        component: () => import('../views/home')
      },
      {
        path: "cart",
        component: () => import('../views/cart')
      },
      {
        path: "list",
        component: () => import('../views/list')
      },
      {
        path: "mine",
        component: () => import('../views/mine')
      }
    ]
  },
  {
    path: "/login",
    component:  () => import('../views/login')
  },
  {
    path: "/detail/:id",
    component:  () => import('../views/detail')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
