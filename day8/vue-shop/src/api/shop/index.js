import axios from '../../utils/request'

function getShopList(query) {
    return axios.get('/shop/list', {params: query})
}
// 获取商品详情
function getShopDetail(id) {
    return axios.get(`/shop/detail/${id}`)
}
export default {
    getShopList,
    getShopDetail
}