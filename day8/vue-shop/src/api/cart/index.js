import axios from '../../utils/request'
// 获取购物车列表
function getCartList(query) {
    return axios.get('/cart/list', {params:query})
}
// 加入购物车
function addToCart(body) {
    return axios.post('/cart/add', body)
}
// 删除购物车商品
function delCart(id) {
    return axios.delete('/cart/delete/'+id)
}
// 修改商品数； 
function setCartCount(body){
    return axios.put(`/cart/setcount`, body)
}
export default {
    getCartList,
    addToCart,
    delCart,
    setCartCount
}