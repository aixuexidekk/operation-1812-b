import axios from '../../utils/request'

function login(body) {
    return axios.post('/login', body)
}
export default {
    login
}