import shop from './shop'
import cart from './cart'
import user from './user'
export default {
    ...shop,
    ...cart,
    ...user
}