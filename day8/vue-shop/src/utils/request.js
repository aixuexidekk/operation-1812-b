import axios from 'axios';
import router from '../router'
import store from '../store'
axios.interceptors.request.use(config=>{
    const token = store.state.user.userData.token
    if(token) {
        config.headers.authorization = "Bearer " + token
    }
    return config
})

axios.interceptors.response.use(response=> {
    return response.data
},err=>{
    console.log(err.response)
    if(err.response) {
        switch(err.response.status) {
            case 401: 
                router.push({path:'/login', query:{redirect: location.pathname}})// 传入离开页面的地址 方便登录成功后再次进入
            break;
        }
    }
    return Promise.reject(err)
})

export default axios;