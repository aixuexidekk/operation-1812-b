/*
 * @Author: your name
 * @Date: 2021-06-22 19:54:20
 * @LastEditTime: 2021-06-22 21:30:34
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \代码git\operation-1812-b\day5\mysql-1\controller\list.js
 */
const query = require('../db/mysql')
const {success,error} =require('../model/model')
class ListController {
    getList = async(ctx) => {
        // id查找 title模糊查询 排序 分页
        // /list?id=1&keyword=123
        const {id, keyword, sortType, sortBy, pageSize,pageIndex} = ctx.query;
        let sql = `select * from shops where 1=1 `
        let sqlCount = `select count(id) as total from shops where 1=1 `
    
        if(id) {
            sql+= `and id=${id} `
            sqlCount+= `and id=${id} `
        }
        if(keyword){
            sql+= `and title like "%${keyword}%" `
            sqlCount+= `and title like "%${keyword}%" `
        }
        if(sortType&&sortBy) {
            sql += `order by ${sortBy},${sortType} `
        }
        if(pageIndex&&pageSize){
            sql += `limit ${(pageIndex-1)*pageSize}, ${pageSize}`
        }
        console.log(sql)
        const data = await query(sql)
        const count = await query(sqlCount)
        ctx.body =success({
            data,
            total: count[0].total
        })
    }
    addList = async(ctx) =>{
        const {title,content,author} = ctx.request.body;
        const sql = `insert into shops(title,content,author,create_time) values("${title}","${content}","${author}","${Date.now()}")`
        const data = await query(sql)
        ctx.body = data;
        if(data.affectedRows===1) {
            ctx.body = success({
                id:data.insertId
            }, '添加成功')
        } else{
            ctx.body = error('添加失败')
        }
        //     "affectedRows": 1, 受到影响的行数
        //     "insertId": 40,  新增数据的id
    }
    editList = async(ctx) =>{
        const {title,content,id} = ctx.request.body;
        const sql = `update shops set title="${title}",content="${content}" where id=${id} `
        const data = await query(sql)
        ctx.body = data;
        if(data.affectedRows>=1) {
            ctx.body = {
                code:1,
                message:"更新成功",
            }
        } else{
            ctx.body = {
                code:-1,
                message:"更新失败",
            }
        }
        //     "affectedRows": 1, 受到影响的行数
        //     "insertId": 40,  新增数据的id
    }
    delList =async(ctx) =>{
        const {id} = ctx.params ;
        const sql = `delete from shops where id="${id}"`;
        const data = await query(sql)
        ctx.body = data;
        if(data.affectedRows===1) {
            ctx.body = {
                code:1,
                message:"删除成功",
            }
        } else{
            ctx.body = {
                code:-1,
                message:"删除失败",
            }
        }
        //     "affectedRows": 1, 受到影响的行数
        //     "insertId": 40,  新增数据的id
    }
}

module.exports = new ListController()