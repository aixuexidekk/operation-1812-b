class Model {
    constructor(data,message) {//4
        if(typeof data === 'string') {
            this.message =data;
            return
        }
        if(data) this.data = data;
        if(message) this.message = message;
    }
}

class SuccessModel extends Model {
    constructor(data,message) {///2
        super(data, message) // 调用父类的constructor //3
        this.code = 1
    }
}
class ErrorModel extends Model {
    constructor(data,message) {
        super(data, message) // 调用父类的constructor
        this.code = -1
    }
}
function success(data,message) {
    return new SuccessModel(data,message)
}
function error(message) {
    return new ErrorModel(message)
}
console.log(
    success("成功"),
    success({},"成功")
)
module.exports = {
    success,
    error
}