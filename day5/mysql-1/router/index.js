const fs = require('fs')
module.exports = (app) => {
    fs.readdir(__dirname, (err,files) => {
        files.forEach((item) => {
            if(item ==='index.js') return
            const router = require(`./${item}`)
            app.use(router.routes())
        })
    })
}