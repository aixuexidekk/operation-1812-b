
const Router = require('koa-router')
const router= new Router()
const {getList,addList,editList,delList} = require('../controller/list.js')
const auth = require('../middleware/auth')
// router.get(url, 中间件s, 控制器)
// 获取博客
router.get('/blog/list', auth,getList)
// 添加博客
router.post('/blog/list',auth,addList)
// 编辑博客
router.put('/blog/list', auth,editList)
// 删除博客
router.delete('/blog/list/:id',auth,delList)

module.exports= router;