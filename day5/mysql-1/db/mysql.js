/*
 * @Author: your name
 * @Date: 2021-06-22 19:54:20
 * @LastEditTime: 2021-06-22 21:12:46
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \代码git\operation-1812-b\day5\mysql-1\db\mysql.js
 */
const mysql=require('mysql')

// 建立连接对象
const con= mysql.createConnection({
    port: "2222",
    host:"localhost",
    user:"root",
    password:"123456",
    database:"shop"
})
// 开启连接
con.connect()
// 封装执行函数   
function query(sql) {
    return new Promise((resolve,reject) => {
        con.query(sql, (err,data) => {
            if(err) {
                reject(err)
            }else {
                resolve(data) 
            }
        })
    })
}

module.exports = {query};