/*
 * @Author: your name
 * @Date: 2021-06-24 09:39:34
 * @LastEditTime: 2021-06-24 13:37:48
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \代码git\operation-1812-b\day5\mysql-1\middleware\auth.js
 */
const jwt =require("jsonwebtoken")
const write = ['/blog/list']
// 鉴权中间件
const auth= async(ctx,next)=>{
    const {authorization} = ctx.request.header;
    if(authorization){
        const token = authorization.split(' ')[1]
        // try  代码运行区
        // catch是 捕获try中的 错误 错误不会导致代码阻塞
        try {
            jwt.verify(token, '1812b')
            await next()
        } catch (error) {
            ctx.status = 401;
            ctx.body={
                code:-1,
                message:"鉴权失败" +error.message
            }
        }
    }else{
        ctx.status = 401;
        ctx.body={
            code:-1,
            message:"你没权限"
        }
    }
}
module.exports= auth;
