const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const cors= require('koa-cors')
const router= require('./router/index')
const app = new Koa()
app.use(cors())
app.use(bodyParser())
router(app)
app.listen(8888,() => {
    console.log(888)
})