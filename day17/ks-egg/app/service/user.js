const Service = require('egg').Service;

class UserService extends Service {
  async login() {
    const {ctx,app} = this;
    const {username,password,nickname} = ctx.request.body;
    const isHave = await app.mysql.get('users', {username,password: require('utility').md5(String(password))})
    if(isHave){
      return {
        data: isHave,
        token: require('jsonwebtoken').sign({...isHave}, app.config.keys)
      }
    } else {
      return '账号密码有误'
    }
  }
  async register() {
    const {ctx,app} = this;
    const {username,password,nickname} = ctx.request.body;
    const isHave = await app.mysql.get('users', {username})
    if(isHave){
      return "用户名已经存在"
    }
    const data = await app.mysql.insert('users',{...ctx.request.body,password: require('utility').md5(String(password))})
    return data
  }
}

module.exports = UserService;
