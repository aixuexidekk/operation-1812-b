module.exports = app => {
    const {router,controller} = app;
    router.post('/register',controller.user.register)
    router.post('/login',controller.user.login)
    // 获取题目列表
    router.get('/list',controller.list.getList)
    // 提交题目
    router.post('/list',controller.list.submit)

}