const Controller = require('egg').Controller;

class UserController extends Controller {
    async getList() {
        const { ctx, app } = this;
        const data = await ctx.service.list.getList();
        if(typeof data === 'string') {
            ctx.body = {
                code: 1,
                message:data
            }
        } else {
            ctx.body = {
                code: 1,
                data
            }
        }
        
    }
    async submit() {
        const { ctx, app } = this;
        const data = await ctx.service.list.submit();
        if(typeof data === 'string') {
            ctx.body = {
                code: 1,
                message:data
            }
        } else {
            ctx.body = {
                code: 1,
                data
            }
        }
        
    }
}

module.exports = UserController;
