import login from '../pages/login'
import exam from '../pages/exam'
import register from '../pages/register'
import result from '../pages/result'

const routes = [
    {
        from: "/",
        to: '/login'
    },
    {
        path: "/login",
        com: login
    },
    {
        path: "/exam",
        com: exam
    },
    {
        path: "/register",
        com: register
    },
    {
        path: "/result",
        com: result
    }
]

export default routes;