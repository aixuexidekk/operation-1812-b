import React from 'react'
import {Route,Switch,Redirect} from 'react-router-dom'
export default function RouterView({routes}) {
    const comList = routes.filter(item => item.com) 
    const redList = routes.filter(item => item.from) 
    return (
        <Switch>
            {comList.map((item) => {
                return <Route key={item.path} path={item.path} render={(history) => {
                    return <item.com {...history} routes={item.children || []}></item.com>
                }}></Route>
            })}
            {redList.map(item=>{
                return <Redirect {...item} key={item.from}></Redirect>
            })}
        </Switch>
    )
}
