import React, {useState,useEffect} from 'react'
import axios from 'axios'
export default function Register({history}) {
    const [username,setUsername] = useState('')
    const [password,setPassword] = useState('')
    const [nickname,setNickname] = useState('')
    return (
        <div>
            <input type="text" value={username} onChange={({target})=> {
                setUsername(target.value)
            }} /> <br />
            <input type="text" value={password} onChange={({target})=> {
                setPassword(target.value)
            }} /> <br />
            <button onClick={() => {
                axios.post('http://localhost:7001/login', {
                    username,
                    password,
                }).then(res=>{
                    if(res.data.code===1){
                        history.push('/exam')
                    }
                })
            }}>登录</button>
        </div>
    )
}
