module.exports = app => {
    const { router, controller,middleware } = app;
    // 获取商品列表
    router.get('/list', controller.shop.getList);
    // 商品详情 /list/2
    router.get('/list/:id', controller.shop.getDetail);
    // 收藏
    router.post('/like', controller.shop.like);


    // 加入购物车
    router.post('/addToCart', middleware.auth(),controller.cart.addToCart);
    // 获取购物车
    router.get('/cart', controller.cart.getCartList);
    // 修改购物车数量
    router.get('/setCartCount', controller.cart.setCartCount);
    // 移除购物车
    router.delete('/cart/:id', controller.cart.delCart);


    // 登录
    router.get('/login', controller.user.login);
    // 注册
    router.get('/register', controller.user.register);
};