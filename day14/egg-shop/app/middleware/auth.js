module.exports = ()=>{
    return async function auth(ctx,next){
        const {authorization} = ctx.request.header;
        if(authorization){
            const {token} = authorization.split(' ')[1]
            try {
                const jwt =require('jsonwebtoken')
                jwt.verify(token,ctx.app.config.keys)
                await next()
            } catch (error) {
                ctx.status= 401;
                ctx.body ={
                    code:-1,
                    message:"健全失败"
                }
            }
        } else{
            ctx.status= 401;
            ctx.body ={
                code:-1,
                message:"无权限"
            }
        }
    }
}