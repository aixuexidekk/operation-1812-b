const Controller = require('../core/base_controller');

class HomeController extends Controller {
  async addToCart() {
    const {ctx,app} = this;
    try{
      const data = await ctx.service.cart.addToCart()
      this.success(data, '加购成功')
    }catch(err) {
      this.error(err.message)
    }
  }
  async getCartList() {
    const {ctx,app} = this;
    try{
      const data = await ctx.service.cart.getCartList()
      this.success(data)
    }catch(err) {
      this.error(err.message)
    }
  }
  
  async setCartCount() {
    this.ctx.body = 'Hello world';
  }
  async delCart() {
    this.ctx.body = 'Hello world';
  }
}

module.exports = HomeController;