const Controller = require('../core/base_controller');
// const mock  = require('mockjs')
// const {list} = mock.mock({
//   "list|300": [
//     {
//       'title':"@ctitle(10,20)",
//       'price|1-1000':1,
//       'image': "https://unsplash.it/400/800/?@name"
//     }
//   ]
// })
// list.forEach(item => {
//   this.app.mysql.insert('shops', item)
// })
class HomeController extends Controller {
  async getList() {
    const {ctx,app} = this;
    try{
      const data = await ctx.service.shop.getList()
      this.success(data)
    }catch(err) {
      this.error(err.message)
    }
  }
  async getDetail() {
    const {ctx,app} = this;
    try{
      const data = await ctx.service.shop.getDetail()
      this.success(data)
    }catch(err) {
      this.error(err.message)
    }
  }
  async like() {
    const {ctx,app} = this;
    try{
      const data = await ctx.service.shop.like()
      this.success(data)
    }catch(err) {
      this.error(err.message)
    }
  }
}

module.exports = HomeController;