const { Controller } = require('egg');
class BaseController extends Controller {
  success(data, message='操作成功') {
      if(data.affectedRows && data.affectedRows>=1) {
        this.ctx.body = {
            code: 1,
            message,
          };
      } else {
        this.ctx.body = {
            code: 1,
            data,
          };
      }
  
  }
  error(msg){
    this.ctx.body = {
        code: -1,
        message:msg,
      };
  }
}
module.exports = BaseController;