//父类
class Modle{
    constructor(data,message){
        if(data) this.data=data;
        if(message) this.message=message;
    }
}
//成功类
class SuccessModlle extends Modle{
    constructor(data,message){
        super(data,message)
        this.code=1
    }
}

//失败类
class ErrorModle extends Modle{
    constructor(message){
        super(null,message)
        this.code=-1;
    }
}
function sucess(data,message){
    return new SuccessModlle(data,message)
}
function error(message){
    return new ErrorModle(message)
}

module.exports={
    sucess,
    error
}