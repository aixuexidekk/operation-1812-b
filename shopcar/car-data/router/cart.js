const router=require('koa-router')();
const {getCartList,addCart,setCount}=require('../controller/cart')

//获取购物车数据
router.get('/api/getcart',getCartList)
//加入购物车
router.post('/api/addcart',addCart)
//修改购物车数量
router.put('/api/edmit',setCount)

module.exports=router;