const Route=require('koa-router');
const router=new Route();
const {Login,Register}=require('../controller/user')


//登录
router.post('/api/login',Login)
//注册
router.post('/api/register',Register)

module.exports=router