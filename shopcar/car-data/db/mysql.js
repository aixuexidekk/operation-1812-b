const mysql=require('mysql')

const con=mysql.createConnection({
    port:'3306',
    host:'localhost',
    user:'root',
    password:'root',
    database:'shopcar'
})
con.connect()  

function query(sql){
    return new Promise((resolve,reject)=>{
        con.query(sql,(err,data)=>{
            if(err)reject(err)
            if(data) resolve(data)
        })
    })
}

module.exports=query

