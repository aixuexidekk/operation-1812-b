import axios from '@/utils/request'

function getShop (query){
    return axios.get('/api/shop',{params:query})
}
function getDetail(id){
    return axios.get(`/api/detail/${id}`)
}

export default {
    getShop,
    getDetail
}