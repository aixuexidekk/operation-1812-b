import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect:'/homes',
    children:[
      {
        path: '/homes',
        name: 'Homes',
        component: () => import(/* webpackChunkName: "Homes" */ '../views/home/Homes.vue')
      },
      {
        path: '/discover',
        name: 'Discover',
        component: () => import(/* webpackChunkName: "Discover" */ '../views/home/Discover.vue')
      },
      {
        path: '/shopcar',
        name: 'Shopcar',
        component: () => import(/* webpackChunkName: "Shopcar" */ '../views/home/Shopcar.vue')
      },
      {
        path: '/my',
        name: 'My',
        component: () => import(/* webpackChunkName: "My" */ '../views/home/My.vue')
      }
    ]
  },
  {
    path: '/detail/:id',
    name: 'Detail',
    component: () => import(/* webpackChunkName: "Detail" */ '../views/Detail.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "Login" */ '../views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
